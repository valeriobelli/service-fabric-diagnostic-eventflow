// Vendor
import React from "react";
import { connect } from "react-redux";
import clsx from "clsx";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import { IconButton, Typography } from "@material-ui/core";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";

// Internal
import { toggleMenu } from "@store/reducers/interface";
import { AppState } from "@store/index";
import LeftMenu from "@components/LeftMenu";
import LogList from "@views/LogList";

const drawerWidth: number = 300;

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
}));

interface Props {
  statoMenu: boolean;
  toggleMenu: typeof toggleMenu;
}

function App(props: Props) {
  const classes = useStyles();

  const toggleMenu = () => {
    props.toggleMenu(!props.statoMenu);
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar 
        position="fixed" 
        className={clsx(classes.appBar, {
          [classes.appBarShift]: props.statoMenu,
        })}>
        <Toolbar>
          <IconButton 
            aria-label="Menu" 
            color="inherit" 
            edge="start" 
            onClick={toggleMenu}
            className={clsx(
              classes.menuButton,
              props.statoMenu && 
              classes.hide
            )}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            Log auditer
          </Typography>
        </Toolbar>
      </AppBar>
      <LeftMenu />
      <LogList />
    </div>
  );
}

const mapStateToProps = (state: AppState) => ({
  statoMenu: state.interfaceReducer.statoMenu,
});

export default connect(mapStateToProps, {
  toggleMenu,
})(App);
