// Vendor
import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

// Internal
import { interfaceReducer, logsReducer } from "@store/reducers/index";
import {
  eventLogHubCommandsMiddleware,
  eventLogHubInvokeMiddleware,
} from "./middlewares/event-log-hub-middleware";

const rootReducer = combineReducers({
  interfaceReducer,
  logsReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
  const middlerwareEnhancements = applyMiddleware(
    ...[thunkMiddleware, eventLogHubInvokeMiddleware],
  );

  const store = createStore(
    rootReducer,
    composeWithDevTools(middlerwareEnhancements),
  );

  /* -------------------------------------------------------------------------- */
  /*                           Register hubs commands                           */
  /* -------------------------------------------------------------------------- */
  eventLogHubCommandsMiddleware(store);

  return store;
}
