// Internal
import { Actions, LogsState, ActionTypes } from "./types"; 

const logsInitialState: LogsState = {
  logs: [],
};

export const logsReducer = (
  state: LogsState = logsInitialState,
  action: ActionTypes
) => {
  switch (action.type) {
    case Actions.AddLogs:
      return {
        ...state,
        logs: [
          ...state.logs,
          ...action.payload
        ],
      };
    default:
      return state;
  }
}