// Internal
import { Actions, ActionTypes, InterfaceState } from "./types";

const initialInterfaceState: InterfaceState = {
  statoMenu: false,
};

export function interfaceReducer(
  state: InterfaceState = initialInterfaceState,
  action: ActionTypes
): InterfaceState {
  switch (action.type) {
    case Actions.TOGGLE_MENU:
      return {
        ...state,
        statoMenu: action.payload,
      } as InterfaceState;
    default:
      return state;
  }
}
