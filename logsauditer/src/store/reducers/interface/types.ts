// Describe the state of the reducer
export interface InterfaceState {
  statoMenu: boolean;
}


// Describe the differente available actions
export const Actions = {
  TOGGLE_MENU: "TOGGLE_MENU",
}

export interface UpdateToggleMenu {
  type: typeof Actions.TOGGLE_MENU;
  payload: boolean;
}

export type ActionTypes = UpdateToggleMenu;
