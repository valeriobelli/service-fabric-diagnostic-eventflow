// Internal
import { Payload } from "./payload";

export interface EventFlowLog {
  timestamp?: Date;
  providerName?: string;
  level?: number;
  keywords?: number;
  payload?: Payload; 
}