export interface Payload {
  message?: string;
  eventId?: number;
  scope?: string;
}