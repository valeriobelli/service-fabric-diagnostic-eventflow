﻿// Standard
using System.Collections.Generic;
using System.Threading.Tasks;

// Vendor
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// Internal
using Eventflow.Contracts;
using WebhookGateway.Interfaces;

namespace WebhookGateway.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class WebhookController : ControllerBase
    {
        private ILogger<WebhookController> Logger { get; }
        private IEventLogGatewayService GatewayService { get; }

        public WebhookController(ILogger<WebhookController> logger, IEventLogGatewayService gatewayService)
        {
            Logger = logger;
            GatewayService = gatewayService;
        }

        [HttpPost("push-log")]
        public async Task<IActionResult> PushLog([FromBody] IList<EventFlowLog> logs)
        {
            using (Logger.BeginScope($"{Request.Method} {Request.Path}"))
            {
                Logger.LogInformation($"PushLog(log={logs}) called.");

                await GatewayService.PushEventLogsToClients(logs);

                return await Task.FromResult(Ok());
            }
        }
    }
}
