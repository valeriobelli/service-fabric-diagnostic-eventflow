﻿// Standard
using System.Collections.Generic;
using System.Threading.Tasks;

// Internal
using Eventflow.Contracts;

namespace WebhookGateway.Interfaces
{
    public interface IEventLogGatewayService
    {
        Task PushEventLogsToClients(IList<EventFlowLog> logs);
    }
}
